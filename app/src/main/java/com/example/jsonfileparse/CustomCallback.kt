package com.example.jsonfileparse

import retrofit2.http.Body

interface CustomCallback {
    fun onFailure(body: String)
    fun onResponse(body: String)
}