package com.example.jsonfileparse

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.high_id_recyclerview_layout.view.*
import kotlinx.android.synthetic.main.low_id_recyclerview_layout.view.*


class RecyclerViewAdapter(
    private val users: MutableList<UserModel.Data>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val lOW_ID = 1
        const val HIGH_ID = 2
    }


    inner class LowIdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {
            val model = users[adapterPosition]
            itemView.firstNameTextView.text = model.firstName
            itemView.lastNameTextView.text = model.lastName
            itemView.emailTextView.text = model.email
            Glide.with(itemView.context).load(model.avatar).into(itemView.firstImageView)

        }
    }

    inner class HighIdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val model = users[adapterPosition]
            itemView.firstName2TextView.text = model.firstName
            itemView.lastName2TextView.text = model.lastName
            itemView.email2TextView.text = model.email
            Glide.with(itemView.context).load(model.avatar).into(itemView.secondImageView)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == lOW_ID){
            LowIdViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.low_id_recyclerview_layout,
                    parent,
                    false))
        } else{
            HighIdViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.high_id_recyclerview_layout,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is LowIdViewHolder)
            holder.onBind()
        else if(holder is HighIdViewHolder)
            holder.onBind()

    }

    override fun getItemViewType(position: Int): Int {
        val model = users[position]
        return if(model.id < 10) {
             lOW_ID
        } else{
            HIGH_ID
        }

    }



}